
Paint app' - made in pure Javascript and canvas 2D
================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/248552d3-b412-4115-be5a-8220f2c10949/small.png)](https://insight.sensiolabs.com/projects/248552d3-b412-4115-be5a-8220f2c10949)

[Demo on paint.chalasdev.fr](http://paint.chalasdev.fr)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

Getting Started
---------------

- Clone this repository in your server directory

``` git clone https://github.com/chalasr/Javascript_My_Paint ```

- Start

``` open your browser & go to localhost/Javascript_my_Paint/ ```

Enjoy drawing !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
