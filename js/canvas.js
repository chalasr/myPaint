$(function(){

	'use strict';
	var canvas = document.getElementById("canvas");
	var click = 1;
	var coord1, coord2, L, H, x, y, color;
	var ctx = canvas.getContext("2d");

	$('#color').change(function(){
		color = $(this).val();
		ctx.strokeStyle = color;
	});

	function getPosition(){
	    var rectangle = canvas.getBoundingClientRect();
	    console.log(rectangle);
	    return{
	        x : event.pageX - rectangle.left,
	        y : event.pageY - rectangle.top
	    };
	};

	function rond(x, y, ray, start, stop){
		ctx.beginPath();
		ctx.arc(x, y, ray, start, stop);
		ctx.stroke();
		ctx.closePath();
		//var ray = Math.sqrt(Math.pow( coord2.x - coord.x, 2) + Math.pow( coord2.y - coord.y, 2));
		//rond(coord.x, coord.y, ray, 0, Math.PI*2);
	};

	function trait(x ,y, x2, y2){
		ctx.beginPath();
		ctx.moveTo(x, y);
		ctx.lineTo(x2, y2);
		ctx.stroke();
		ctx.closePath();
	};

	// function dessin(x)

	function rectangle(x ,y, L, H){
		//if(inputname'plein' = plein) ? ctx.fillRect()
	    ctx.strokeRect(x, y, L, H);
	};

	$(function(){
		$('#clear').click(function(){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
		});
		$('#btnrond').click(function(){
			$('.btn-default').removeClass('active');
			$('#btnrond').toggleClass('active');
		});
		$('#btnrect').click(function(){
			$('.btn-default').removeClass('active');
			$('#btnrect').toggleClass('active');
		});
		$('#btntrait').click(function(){
			$('.btn-default').removeClass('active');
			$('#btntrait').toggleClass('active');
		});
	});

	canvas.addEventListener("click", function(){
	    if(click == 1){
	        coord1 = getPosition();
	        click++;
	        console.log(coord1);
	    }else{
	    	coord2 = getPosition();
	    	if($('input[name=menu]:checked').val() == 'rond'){
				var ray = Math.sqrt(Math.pow(coord2.x - coord1.x, 2) + Math.pow( coord2.y - coord1.y, 2));
				rond(coord1.x, coord1.y, ray, 0, Math.PI*2);
				click = 1;
				console.log('rond');
			}else if($('input[name=menu]:checked').val() == 'rectangle'){
		        rectangle(coord1.x, coord1.y, coord2.x - coord1.x, coord2.y - coord1.y);
		        click = 1;
		        console.log('rectangle');
		    }else if($('input[name=menu]:checked').val() == 'trait'){
		        trait(coord1.x, coord1.y, coord2.x , coord2.y);
		        click = 1;
		        console.log(coord1);
		    }
	    }
	});
});